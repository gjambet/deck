package fr.univsmb.isc;

import java.util.Set;

public class Deck {

    private final Set<Card> cards ;

    public Deck(int nbCards) {
        cards = new CardSetBuilder().with(32).shuffled();
    }

    public Card draw() throws EmptyDeckException {

        if (cards.isEmpty()) {
            throw new EmptyDeckException();
        }

        Card card = cards.iterator().next();
        cards.remove(card);
        return card;

    }

    public boolean contains(Card c) {
        return cards.contains(c);
    }
}
