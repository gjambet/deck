package fr.univsmb.isc;

import java.util.*;

public class CardSetBuilder {

    private List cards;

    public CardSetBuilder() {
        this.cards = new ArrayList();
    }

    public CardSetBuilder with(int nb) {
        int nbCards = nb / 4;
        cards.addAll(buildFamily("Spade", nbCards));
        cards.addAll(buildFamily("Club", nbCards));
        cards.addAll(buildFamily("Diamond", nbCards));
        cards.addAll(buildFamily("Heart", nbCards));
        return this;
    }

    private List<Card> buildFamily(String color, int max) {
        List<Card> family = new ArrayList<Card>();
        int count = 0;

        buildCard(count, max, color, "Ace", 14, family);
        buildCard(count, max, color, "King", 13, family);
        buildCard(count, max, color, "Queen", 12, family);
        buildCard(count, max, color, "Jack", 11, family);
        buildCard(count, max, color, "10", 10, family);
        buildCard(count, max, color, "9", 9, family);
        buildCard(count, max, color, "8", 8, family);
        buildCard(count, max, color, "7", 7, family);

        return family;
    }

    private void buildCard(int count, int max, String color, String name, int value, List<Card> family) {
        if (count < max) {
            family.add(new Card(name, color, value));
        }
    }


    public Set<Card> shuffled() {
        Collections.shuffle(cards);
        return new HashSet<Card>(cards);
    }
}
