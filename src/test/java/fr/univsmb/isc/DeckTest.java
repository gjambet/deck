package fr.univsmb.isc;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DeckTest {

    private Deck deck;

    @Before
    public void setUp() {
        deck = new Deck(32);
    }

    @Test
    public void shouldDrawACard() throws Exception {
        Card card = deck.draw();
        assertThat(card).isNotNull();
    }

    @Test(expected = EmptyDeckException.class)
    public void shouldNotBeAbleToDrawMoreCardThanWhatSInTheDeck() throws EmptyDeckException {
        for (int i = 0; i <= 33; i++) {
            deck.draw();
        }
    }

    @Test
    public void shouldNotContainsAnymoreADrawnCard() throws EmptyDeckException {
        assertThat(deck.contains(deck.draw())).isFalse();
    }

}