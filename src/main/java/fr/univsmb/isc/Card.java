package fr.univsmb.isc;

import java.util.Objects;

public class Card {

    public final String name;
    public final String color;
    public final int value;

    public Card(String name, String color, int value) {
        this.name = name;
        this.color = color;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return value == card.value &&
                Objects.equals(name, card.name) &&
                Objects.equals(color, card.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, color, value);
    }
}
